Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post 'auth/register', to: 'users#register'

  # [...]
  post 'auth/login', to: 'users#login'
  # [...]

  resources :flowers, :only=>[:index] do
    resources :sightings, :only=>[:index, :create, :destroy] do
      resources :likes, :only => [:create, :destroy]
    end
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
end
