class QuestionWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(sighting_id)
    sighting = Sighting.find(sighting_id)
    question = RandomQuestionGenerator.new.get(sighting.id)
    sighting.question = question
    sighting.save
  end
end
