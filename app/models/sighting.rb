class Sighting < ApplicationRecord
  belongs_to :user
  belongs_to :flower
  has_one_attached :image
  has_many :likes

  after_create do
    self.question = RandomQuestionGenerator.new.get(self.id)
    self.save
  end
end
