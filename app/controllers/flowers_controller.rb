class FlowersController < ApplicationController
  skip_before_action :authenticate_request, only: %i[index]

  def index
    flowers_list = Flower.all
    render json: flowers_list, status: :ok
  end
end