class SightingsController < ApplicationController
  skip_before_action :authenticate_request, only: %i[index]
  before_action :find_flower

  def index
    render json: @flower.sightings
  end

  def create
    @sighting = @flower.sightings.new(sighting_params)
    @sighting.user_id = @current_user.id

    if @sighting.save
      response = { message: 'Sighting was created successfully'}
      render json: response, status: :created
    else
      render json: @sighting.errors, status: :bad_request
    end
  end

  def destroy
    @sighting = @current_user.sightings.find(params[:id])
    if @sighting.destroy
      response = { message: 'Sighting was destroyed successfully.'}
      render json: response, status: :ok
    else
      response = { message: 'Something went wrong!'}
      render json: response, status: :bad_request
    end
  end

  private

  def find_flower
    @flower = Flower.find(params[:flower_id])
  end

  def sighting_params
    params.permit(:latitude,:longitude,:image)
  end
end
