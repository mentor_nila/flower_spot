class LikesController < ApplicationController
  def create
    @like = @sighting.likes.new(user_id: @current_user.id)
    if @like.save
      response = { message: 'You have successfully liked sighting you have clicked!'}
      render json: response, status: :created
    else
      render json: @like.errors, status: :bad
    end
  end

  def destroy
    @like = @current_user.likes.find(params[:id])
    if @like.destroy
      response = { message: 'Your like was removed!'}
      render json: response, status: :ok
    else
      response = { message: 'Your like was removed!'}
      render json: response, status: :bad_request
    end
  end

  private

  def find_sighting
    @sighting = Sighting.find(params[:sighting_id])
  end

end
