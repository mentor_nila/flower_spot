class RandomQuestionGenerator
  require 'json'

  def get(sighting_id)
    begin
      response = HTTParty.get('https://opentdb.com/api.php?amount=1')
      JSON.parse(response.body)['results'][0]['question']
    rescue Net::ReadTimeout
      QuestionWorker.perform_in(1.hour, sighting_id)
      nil
    end
  end

end